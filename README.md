# dot-env

A storage place for all my personal computer configuration, to be used when setting up a new computer 

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Installation
todo, detail how to get everything done on a new system

## ToDo:
- Make script to install everything on machine
- plugin in custom scripts
- Setup zsh (https://github.com/ohmyzsh/ohmyzsh)
  - Finish setting up 10k theme. 2 lines, 1 display second for command. 
		- https://github.com/romkatv/powerlevel10k#instant-prompt
  - Find good plugins. 
    - https://github.com/unixorn/awesome-zsh-plugins#oh-my-zsh
    - https://github.com/unixorn/awesome-zsh-plugins
